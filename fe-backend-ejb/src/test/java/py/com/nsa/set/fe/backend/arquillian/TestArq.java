package py.com.nsa.set.fe.backend.arquillian;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
@RunWith(Arquillian.class)
public class TestArq {

    @Deployment
    public static JavaArchive createDeployment() {

        //creamos el modulo postventa-ejb-test.jar que va contener todos los servicios ejb de nuestro modulo
        JavaArchive ejbModule = ShrinkWrap.create(JavaArchive.class, "fe-backend-ejb-test.jar");
        /*ejbModule.addClass(FeService.class);
        ejbModule.addPackages(true, "py.com.nsa.set.fe.backend.services");
        //agregamos nuestro propio beans.xml
        ejbModule.addAsManifestResource(new File("src\\main\\resources\\META-INF\\beans.xml"));*/
        //MyBatis Config, se agregan los archivos de configuracion en la misma posiscion que en el jar original

        /*File myBatisConfigurationProperties = new File("src\\main\\resources\\configuraciones\\MyBatisConfiguration.properties");
        File myBatisConfigurationXml = new File("src\\main\\resources\\configuraciones\\MyBatisConfiguration.xml");

        ArchivePath myBatisConfigurationPropertiesTargetPath = new BasicPath("configuraciones/MyBatisConfiguration.properties");
        ArchivePath myBatisConfigurationXmlTargetPath        = new BasicPath("configuraciones/MyBatisConfiguration.xml");

        ejbModule.addAsResource(myBatisConfigurationProperties, myBatisConfigurationPropertiesTargetPath);
        ejbModule.addAsResource(myBatisConfigurationXml, myBatisConfigurationXmlTargetPath);*/


        return ejbModule;
    }

    @Test
    public void should_create_greeting() {
        Assert.fail("Not yet implemented");
    }
}
