package py.com.nsa.set.fe.backend.services;

import https.app_nsa_com_py.fe.si_recepde.DE;
import https.app_nsa_com_py.fe.si_recepde.GCOpeDE;
import org.apache.xml.security.exceptions.XMLSecurityException;
import py.com.daas.mybatis.backendbase.utils.ServiceUtils;
import py.com.daas.util.exceptions.ApplicationException.IllegalArgument;
import py.com.daas.util.exceptions.ApplicationException.NotFound;
import py.com.daas.util.exceptions.ApplicationException.SQLException;
import py.com.daas.util.logging.interceptors.LoggingInterceptor;
import py.com.nsa.nsaweb.commons.dao.ComprobanteClienteDAO;
import py.com.nsa.nsaweb.commons.models.ComprobanteCliente;
import py.com.nsa.set.fe.backend.dao.DocElectClienteDAO;
import py.com.nsa.set.fe.backend.models.DocElectClienteWithBLOBs;
import py.com.nsa.set.fe.backend.models.params.fe.ProcesarCbteParam;
import py.com.nsa.set.fe.backend.services.sign.CreateSignature;
import py.com.nsa.set.fe.backend.services.utils.DigitoVerificador;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Date;

import static py.com.nsa.set.fe.backend.services.Constants.COD_SEG_FINAL;
import static py.com.nsa.set.fe.backend.services.Constants.COD_SEG_INICIAL;
import static py.com.nsa.set.fe.backend.services.Constants.DE.AMB_PRO;
import static py.com.nsa.set.fe.backend.services.Constants.DE.DES_TIP_EMI;
import static py.com.nsa.set.fe.backend.services.Constants.DE.MOD_VAL_POS;
import static py.com.nsa.set.fe.backend.services.Constants.DE.NSA_RUC;
import static py.com.nsa.set.fe.backend.services.Constants.DE.TIEM_VAL_POS_48;
import static py.com.nsa.set.fe.backend.services.Constants.DE.TIP_EMI;
import static py.com.nsa.set.fe.backend.services.Constants.DE_PREFIX;

/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
@Stateless
@Interceptors(LoggingInterceptor.class)
public class FeService extends BaseService {

    @Inject
    private ComprobanteClienteDAO comprobanteClienteDAO;

    @Inject
    private DocElectClienteDAO docElectClienteDAO;

    public DE procesarCbte(ProcesarCbteParam param)
            throws IllegalArgument, SQLException, NotFound  {

        ComprobanteCliente comprobanteCliente = comprobanteClienteDAO.obtener(param.getCbteCliKey());
        ServiceUtils.validarEncontrado(comprobanteCliente, "No existe comprobante");

        DE de;
        byte[] deXml;

        try {
            de = prepararDE(comprobanteCliente);
            deXml = CreateSignature.create(de);
        } catch (CertificateException | UnrecoverableKeyException | NoSuchAlgorithmException | XMLSecurityException |
                KeyStoreException| TransformerException | IOException e) {
            throw new IllegalArgument("No se pudo crear DE", e);
        }

        DocElectClienteWithBLOBs docElectCliente = new DocElectClienteWithBLOBs();
        docElectCliente.setFechaCreacion(new Date());
        docElectCliente.setFechaFirma(new Date());
        docElectCliente.setFechaSet(new Date());
        docElectCliente.setSxml(deXml);
        docElectCliente.setSxmlfirmado(deXml);
        docElectCliente.setCodTipoDe(new BigDecimal("1"));
        docElectCliente.setOrigen(new BigDecimal("1"));
        docElectClienteDAO.insertarPK(docElectCliente);

        return de;
    }

    private DE prepararDE(ComprobanteCliente cbte) {
        DE de = new DE();
        de.setDVerFor(new BigInteger("100"));
        de.setDIDE(getDIDE());

        GCOpeDE gcOpeDE = new GCOpeDE();
        gcOpeDE.setIAmbDE(AMB_PRO);
        gcOpeDE.setIModVal(MOD_VAL_POS);
        gcOpeDE.setITiemValPos(TIEM_VAL_POS_48);
        gcOpeDE.setITipEmi(TIP_EMI);
        gcOpeDE.setDDesTipEmi(DES_TIP_EMI);
        Integer codSeg = obtenerCodSeg();
        gcOpeDE.setICodSe(codSeg);
        de.setGCOpeDE(gcOpeDE);

        return de;
    }

    private String getDIDE() {
        String feB2B           = "01";
        String nroDocDVEmisor  = NSA_RUC;
        String establecimiento = "123";
        String ptoExp          = "123";
        String nroDoc          = "1234567";
        String tipoContrib     = "2";
        String fechaEmi        = "24052018";
        String tipoEmi         = "1";
        String codSeg          = String.valueOf(obtenerCodSeg());//todo preguntar los rangos (necesita padding)

        String stream = feB2B + nroDocDVEmisor + establecimiento + ptoExp + nroDoc + tipoContrib + fechaEmi + tipoEmi + codSeg;
        String div    = DigitoVerificador.getDV(stream);

        stream = DE_PREFIX + stream + div;

        return stream;
    }

    private Integer obtenerCodSeg() {
        Integer numero = (int) (Math.random() * COD_SEG_FINAL) + COD_SEG_INICIAL;
        return numero;
    }

}
