package py.com.nsa.set.fe.backend.services.sign;

import https.app_nsa_com_py.fe.si_recepde.DE;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignature;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Constants;
import org.w3c.dom.Document;
import py.com.daas.util.exceptions.ApplicationException.IllegalArgument;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import static org.apache.xml.security.algorithms.MessageDigestAlgorithm.ALGO_ID_DIGEST_SHA256;


/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
public class CreateSignature {

    private static final String KEYSTORE_TYPE = "JKS";
    private static final String KEYSTORE_FILE = "F:\\Projects\\myKeyStore.jks";
    private static final String KEYSTORE_PASSWORD = "abc12345";
    private static final String PRIVATE_KEY_PASSWORD = "abc1234";
    private static final String PRIVATE_KEY_ALIAS = "mi_cert_ejemplo";

    private static final String DES_FOLDER = "F:\\Projects\\NSA\\java\\fe-backend\\des\\";

    /**
     * Punto de entrada al ejemplo
     */
    public static byte[] create(DE de) throws IllegalArgument, XMLSecurityException, KeyStoreException,
            IOException, CertificateException, NoSuchAlgorithmException, UnrecoverableKeyException, TransformerException {

        org.apache.xml.security.Init.init();
        Document doc = DomUtils.createDocument(de);
        Constants.setSignatureSpecNSprefix("");    // Sino, pone por defecto como prefijo: "ns"

        // Cargamos el almacen de claves
        KeyStore ks = KeyStore.getInstance(KEYSTORE_TYPE);
        ks.load(new FileInputStream(KEYSTORE_FILE), KEYSTORE_PASSWORD.toCharArray());

        // Obtenemos la clave privada, pues la necesitaremos para encriptar.
        PrivateKey privateKey    = (PrivateKey) ks.getKey(PRIVATE_KEY_ALIAS, PRIVATE_KEY_PASSWORD.toCharArray());
        File       signatureFile = new File(DES_FOLDER + de.getDIDE() + ".xml");
        String     baseURI       = signatureFile.toURL().toString();    // BaseURI para las URL Relativas.

        // Instanciamos un objeto XMLSignature desde el Document. El algoritmo de firma será RSA
        XMLSignature xmlSignature = new XMLSignature(doc, baseURI, XMLSignature.ALGO_ID_SIGNATURE_RSA_SHA256);

        // Añadimos el nodo de la firma a la raiz antes de firmar.
        // Observe que ambos elementos pueden ser mezclados en una forma con referencias separadas
        doc.getDocumentElement().appendChild(xmlSignature.getElement());

        // Creamos el objeto que mapea: Document/Reference
        Transforms transforms = new Transforms(doc);
        transforms.addTransform(Transforms.TRANSFORM_ENVELOPED_SIGNATURE);

        // Añadimos lo anterior Documento / Referencia
        // ALGO_ID_DIGEST_SHA1 = "http://www.w3.org/2000/09/xmldsig#sha1";
        xmlSignature.addDocument("#" + de.getDIDE(), transforms, ALGO_ID_DIGEST_SHA256);

        // Añadimos el KeyInfo del certificado cuya clave privada usamos
        X509Certificate cert = (X509Certificate) ks.getCertificate(PRIVATE_KEY_ALIAS);
        xmlSignature.addKeyInfo(cert);
        //xmlSignature.addKeyInfo(cert.getPublicKey());

        // Realizamos la firma
        xmlSignature.sign(privateKey);

        // Guardamos archivo de firma en disco
        DomUtils.outputDocToFile(doc, signatureFile);

        byte[] mirar = DomUtils.outputDocToByteArray(doc);

        return mirar;
    }

}
