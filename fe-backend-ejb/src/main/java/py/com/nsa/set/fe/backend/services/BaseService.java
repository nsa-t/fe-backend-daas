package py.com.nsa.set.fe.backend.services;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import py.com.daas.mybatis.backendbase.services.ServiceBase;

import javax.inject.Inject;

/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
public class BaseService extends ServiceBase {

    @Inject
    private SqlSessionFactory factory;

    @Override
    protected SqlSession getSqlSession() {
        return factory.openSession();
    }
}
