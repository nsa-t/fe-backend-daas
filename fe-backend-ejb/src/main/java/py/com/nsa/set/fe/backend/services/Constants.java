package py.com.nsa.set.fe.backend.services;

/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
public class Constants {

    public class DE {

        public static final String NSA_RUC = "800025776";

        public static final String VER_FOR = "100";

        public static final short AMB_PRO = 1;
        public static final short AMB_DEV = 2;

        public static final String DES_AMB_PRO = "PRODUCCION";
        public static final String DES_AMB_DEV = "PRUEBA";

        public static final short MOD_VAL_POS = 1;
        public static final short MOD_VAL_PRE = 2;

        public static final String DES_MOD_VAL_POS = "POSTERIOR";
        public static final String DES_MOD_VAL_PRE = "PREVIA";

        public static final String VAL_POS_24HS = "1";
        public static final String VAL_POS_48HS = "2";
        public static final String VAL_POS_72HS = "3";

        public static final short TIEM_VAL_POS_24 = 24;
        public static final short TIEM_VAL_POS_48 = 48;
        public static final short TIEM_VAL_POS_72 = 72;

        public static final short TIP_EMI = 1;
        public static final String DES_TIP_EMI = "NORMAL";



    }

    public static final String FE_B2B = "01";

    public static final String DE_PREFIX = "DE";
    public static final Integer COD_SEG_INICIAL = 100000000;
    public static final Integer COD_SEG_FINAL = 999999999;
}
