package py.com.nsa.set.fe.backend.services.sign;

import https.app_nsa_com_py.fe.si_recepde.DE;
import https.app_nsa_com_py.fe.si_recepde.GCOpeDE;
import https.app_nsa_com_py.fe.si_recepde.GCamFuFD;
import https.app_nsa_com_py.fe.si_recepde.RDE;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import py.com.daas.util.exceptions.ApplicationException.IllegalArgument;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;


/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
public class DomUtils {

    /**
     * Serializa un objeto Document en un archivo
     */
    public static void outputDocToFile(Document doc, File file) throws IOException, TransformerException {
        FileOutputStream   f           = new FileOutputStream(file);
        TransformerFactory factory     = TransformerFactory.newInstance();
        Transformer        transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(new DOMSource(doc), new StreamResult(f));

        f.close();
    }

    public static byte[] outputDocToByteArray(Document doc) throws TransformerException {
        ByteArrayOutputStream b         = new ByteArrayOutputStream();
        TransformerFactory    factory     = TransformerFactory.newInstance();
        Transformer           transformer = factory.newTransformer();

        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.transform(new DOMSource(doc), new StreamResult(b));

        return b.toByteArray();
    }

    /**
     * Lee un Document desde un archivo
     */
    public static Document loadDocumentFromFile(File file) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder        builder = null;

        factory.setNamespaceAware(true);

        builder = factory.newDocumentBuilder();

        return builder.parse(file);
    }

    /**
     * @return Crea un elemento <tag>value</tag>
     */
    public static Element createNode(Document document, String tag, String value) {
        Element node = document.createElement(tag);
        if (value != null) {
            node.appendChild(document.createTextNode(value));
        }
        return node;
    }

    public static Document createDocument(DE de) throws IllegalArgument {
        DOMResult   res     = new DOMResult();
        JAXBContext context = null;
        try {
            context = JAXBContext.newInstance(de.getClass());
            context.createMarshaller().marshal(de, res);
        } catch (JAXBException e) {
            throw new IllegalArgument(e);
        }

        Document doc     = (Document) res.getNode();
        Element  element = (Element) doc.getElementsByTagName("dIDE").item(0);
        element.setAttributeNS(null, "Id", de.getDIDE());
        return doc;
    }

    private static DE createDE() {
        DE de = new DE();
        de.setDVerFor(new BigInteger("100"));
        de.setDIDE("DE01444444017001001001452822017012515873260988");

        GCOpeDE gcOpeDE = new GCOpeDE();
        gcOpeDE.setIAmbDE(new Short("1"));
        gcOpeDE.setIModVal(new Short("1"));
        gcOpeDE.setITiemValPos(new Short("48"));
        gcOpeDE.setITipEmi(new Short("1"));
        gcOpeDE.setDDesTipEmi("NORMAL");
        gcOpeDE.setICodSe(new Short("45"));
        gcOpeDE.setIIndPres(new Short("1"));
        gcOpeDE.setDDesIndPres("OPERACION PRESENCIAL");
        gcOpeDE.setITipTra(new Short("1"));
        gcOpeDE.setDDesTiTran("VENTA DE MERCADERIA");
        gcOpeDE.setDInfoEmi("VENTA DE MERCADERIA");
        de.setGCOpeDE(gcOpeDE);

        return de;
    }

    private static RDE createRDE() {
        RDE rde = new RDE();
        rde.setDE(createDE());

        GCamFuFD gCamFuFD = new GCamFuFD();
        gCamFuFD.setDCarQR("1212212121212122222222121212122101212121212121201ASD4444440170010010SFSG0145282GJFH2501201HJHGJK71587322IYUI6094");
        gCamFuFD.setDInfAdic("esta información es adicional");

        rde.setGCamFuFD(gCamFuFD);
        return rde;
    }

}
