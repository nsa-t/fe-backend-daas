package py.com.nsa.set.fe.backend.sqlsession;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import java.io.IOException;
import java.io.InputStream;

public class SqlSessionFactoryProvider {
    
    /**
     * Obtiene  una instancia SqlSessionFactory
     * @throws IOException
     */
	@Produces
	@ApplicationScoped
    public SqlSessionFactory getSqlSessionFactory() throws IOException {
        String resource = "configuraciones/MyBatisConfiguration.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        return new SqlSessionFactoryBuilder().build(inputStream);
    }
}
