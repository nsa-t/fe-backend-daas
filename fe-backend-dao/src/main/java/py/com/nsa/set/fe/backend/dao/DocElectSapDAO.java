package py.com.nsa.set.fe.backend.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import py.com.daas.mybatis.backendbase.dao.MyBatisGenericDAO;
import py.com.nsa.set.fe.backend.models.DocElectSap;
import py.com.nsa.set.fe.backend.models.examples.DocElectSapExample;
import py.com.nsa.set.fe.backend.models.mappers.DocElectSapMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class DocElectSapDAO extends MyBatisGenericDAO<DocElectSap, Short, DocElectSapExample, DocElectSapMapper> {
    @Inject
    private SqlSessionFactory sqlSessionFactory;

    @Override
    protected Class<DocElectSapMapper> getMapperType() {
        return DocElectSapMapper.class;
    }

    @Override
    protected Class<DocElectSap> getEntityBeanType() {
        return DocElectSap.class;
    }

    @Override
    protected SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }
}