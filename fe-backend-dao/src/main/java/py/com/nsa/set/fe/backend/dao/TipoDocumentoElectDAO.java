package py.com.nsa.set.fe.backend.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import py.com.daas.mybatis.backendbase.dao.MyBatisGenericDAO;
import py.com.nsa.set.fe.backend.models.TipoDocumentoElect;
import py.com.nsa.set.fe.backend.models.examples.TipoDocumentoElectExample;
import py.com.nsa.set.fe.backend.models.mappers.TipoDocumentoElectMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class TipoDocumentoElectDAO extends MyBatisGenericDAO<TipoDocumentoElect, Short, TipoDocumentoElectExample, TipoDocumentoElectMapper> {
    @Inject
    private SqlSessionFactory sqlSessionFactory;

    @Override
    protected Class<TipoDocumentoElectMapper> getMapperType() {
        return TipoDocumentoElectMapper.class;
    }

    @Override
    protected Class<TipoDocumentoElect> getEntityBeanType() {
        return TipoDocumentoElect.class;
    }

    @Override
    protected SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }
}