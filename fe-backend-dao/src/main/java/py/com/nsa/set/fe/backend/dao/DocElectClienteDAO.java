package py.com.nsa.set.fe.backend.dao;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import py.com.daas.mybatis.backendbase.dao.MyBatisGenericDAO;
import py.com.daas.mybatis.backendbase.utils.ServiceUtils;
import py.com.daas.util.exceptions.ApplicationException.IllegalArgument;
import py.com.daas.util.exceptions.ApplicationException.SQLException;
import py.com.nsa.set.fe.backend.models.DocElectCliente;
import py.com.nsa.set.fe.backend.models.DocElectClienteWithBLOBs;
import py.com.nsa.set.fe.backend.models.examples.DocElectClienteExample;
import py.com.nsa.set.fe.backend.models.mappers.DocElectClienteMapper;

import javax.ejb.Stateless;
import javax.inject.Inject;

@Stateless
public class DocElectClienteDAO extends MyBatisGenericDAO<DocElectCliente, Short, DocElectClienteExample, DocElectClienteMapper> {
    @Inject
    private SqlSessionFactory sqlSessionFactory;

    @Override
    protected Class<DocElectClienteMapper> getMapperType() {
        return DocElectClienteMapper.class;
    }

    @Override
    protected Class<DocElectCliente> getEntityBeanType() {
        return DocElectCliente.class;
    }

    @Override
    protected SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    public Short insertarPK(DocElectClienteWithBLOBs entity) throws IllegalArgument, SQLException {
        ServiceUtils.validarArgumento(entity != null, "Se requiere la entidad");
        this.beforeInsertValidation(entity);

        try {
            SqlSession sqlSession = this.getSqlSession();
            Throwable throwable = null;

            try {
                DocElectClienteMapper mapper = sqlSession.getMapper(this.getMapperType());
                entity.setIdDocElectCliente(mapper.getIdDocElectCliente());
                mapper.insert(entity);
            } catch (Throwable t) {
                throwable = t;
                throw t;
            } finally {
                this.closeSqlSession(throwable);
            }

            return entity.getIdDocElectCliente();
        } catch (PersistenceException e) {
            throw new SQLException(e.getCause());
        }
    }
}