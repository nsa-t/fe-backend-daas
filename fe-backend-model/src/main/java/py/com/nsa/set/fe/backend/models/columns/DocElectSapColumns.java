package py.com.nsa.set.fe.backend.models.columns;

import java.util.HashMap;
import java.util.Map;

public final class DocElectSapColumns {
    private static final Map<String, String> mapColumn = new HashMap<String, String>();

    static {
        mapColumn.put("idDocElectCliente","ID_DOC_ELECT_CLIENTE");
        mapColumn.put("docentry","DOCENTRY");
    }

    public static String getColumnsName(String nombreAtributo) {
        return mapColumn.get(nombreAtributo);
    }
}