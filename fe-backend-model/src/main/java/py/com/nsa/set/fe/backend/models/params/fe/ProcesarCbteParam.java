package py.com.nsa.set.fe.backend.models.params.fe;


import py.com.nsa.nsaweb.commons.models.ComprobanteClienteKey;

/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
public class ProcesarCbteParam {

    private Integer ptoVta;
    private Short cbteTipo;
    private ComprobanteClienteKey cbteCliKey;

    public Integer getPtoVta() {
        return ptoVta;
    }

    public void setPtoVta(Integer ptoVta) {
        this.ptoVta = ptoVta;
    }

    public Short getCbteTipo() {
        return cbteTipo;
    }

    public void setCbteTipo(Short cbteTipo) {
        this.cbteTipo = cbteTipo;
    }

    public ComprobanteClienteKey getCbteCliKey() {
        return cbteCliKey;
    }

    public void setCbteCliKey(ComprobanteClienteKey cbteCliKey) {
        this.cbteCliKey = cbteCliKey;
    }

}
