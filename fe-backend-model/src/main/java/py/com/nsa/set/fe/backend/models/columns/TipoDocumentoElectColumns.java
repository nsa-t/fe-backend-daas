package py.com.nsa.set.fe.backend.models.columns;

import java.util.HashMap;
import java.util.Map;

public final class TipoDocumentoElectColumns {
    private static final Map<String, String> mapColumn = new HashMap<String, String>();

    static {
        mapColumn.put("codTipoDe","COD_TIPO_DE");
        mapColumn.put("descripcipon","DESCRIPCIPON");
        mapColumn.put("codigoSet","CODIGO_SET");
        mapColumn.put("habilitado","HABILITADO");
        mapColumn.put("cliProv","CLI_PROV");
    }

    public static String getColumnsName(String nombreAtributo) {
        return mapColumn.get(nombreAtributo);
    }
}