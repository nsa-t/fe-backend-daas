package py.com.nsa.set.fe.backend.models.params;

/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
public class ClienteParams {
    private Integer codAgencia;
    private Long codCliente;

    public Integer getCodAgencia() {
        return codAgencia;
    }

    public void setCodAgencia(Integer codAgencia) {
        this.codAgencia = codAgencia;
    }

    public Long getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(Long codCliente) {
        this.codCliente = codCliente;
    }
}
