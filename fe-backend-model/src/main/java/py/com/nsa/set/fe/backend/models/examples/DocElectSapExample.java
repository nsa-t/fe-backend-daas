package py.com.nsa.set.fe.backend.models.examples;

import py.com.daas.mybatis.backendbase.examples.MyBatisExample;
import py.com.daas.util.exceptions.MybatisSqlException;
import py.com.nsa.set.fe.backend.models.DocElectSap;
import py.com.nsa.set.fe.backend.models.columns.DocElectSapColumns;

import java.util.ArrayList;
import java.util.List;

public class DocElectSapExample implements MyBatisExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public DocElectSapExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public String getColumnNameByAtribute(String atribute) throws MybatisSqlException {
        String nombreColumna = DocElectSapColumns.getColumnsName(atribute);
        if (nombreColumna == null) {
            throw new MybatisSqlException.IllegalArgument(
            "No se encontro NOMBRE de COLUMNA para el Atributo "
            + atribute + " " + DocElectSap.class);
        }
        return nombreColumna;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdDocElectClienteIsNull() {
            addCriterion("ID_DOC_ELECT_CLIENTE is null");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteIsNotNull() {
            addCriterion("ID_DOC_ELECT_CLIENTE is not null");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteEqualTo(Short value) {
            addCriterion("ID_DOC_ELECT_CLIENTE =", value, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteNotEqualTo(Short value) {
            addCriterion("ID_DOC_ELECT_CLIENTE <>", value, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteGreaterThan(Short value) {
            addCriterion("ID_DOC_ELECT_CLIENTE >", value, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteGreaterThanOrEqualTo(Short value) {
            addCriterion("ID_DOC_ELECT_CLIENTE >=", value, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteLessThan(Short value) {
            addCriterion("ID_DOC_ELECT_CLIENTE <", value, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteLessThanOrEqualTo(Short value) {
            addCriterion("ID_DOC_ELECT_CLIENTE <=", value, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteIn(List<Short> values) {
            addCriterion("ID_DOC_ELECT_CLIENTE in", values, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteNotIn(List<Short> values) {
            addCriterion("ID_DOC_ELECT_CLIENTE not in", values, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteBetween(Short value1, Short value2) {
            addCriterion("ID_DOC_ELECT_CLIENTE between", value1, value2, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andIdDocElectClienteNotBetween(Short value1, Short value2) {
            addCriterion("ID_DOC_ELECT_CLIENTE not between", value1, value2, "idDocElectCliente");
            return (Criteria) this;
        }

        public Criteria andDocentryIsNull() {
            addCriterion("DOCENTRY is null");
            return (Criteria) this;
        }

        public Criteria andDocentryIsNotNull() {
            addCriterion("DOCENTRY is not null");
            return (Criteria) this;
        }

        public Criteria andDocentryEqualTo(Short value) {
            addCriterion("DOCENTRY =", value, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryNotEqualTo(Short value) {
            addCriterion("DOCENTRY <>", value, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryGreaterThan(Short value) {
            addCriterion("DOCENTRY >", value, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryGreaterThanOrEqualTo(Short value) {
            addCriterion("DOCENTRY >=", value, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryLessThan(Short value) {
            addCriterion("DOCENTRY <", value, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryLessThanOrEqualTo(Short value) {
            addCriterion("DOCENTRY <=", value, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryIn(List<Short> values) {
            addCriterion("DOCENTRY in", values, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryNotIn(List<Short> values) {
            addCriterion("DOCENTRY not in", values, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryBetween(Short value1, Short value2) {
            addCriterion("DOCENTRY between", value1, value2, "docentry");
            return (Criteria) this;
        }

        public Criteria andDocentryNotBetween(Short value1, Short value2) {
            addCriterion("DOCENTRY not between", value1, value2, "docentry");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated do_not_delete_during_merge Mon Jun 18 13:34:19 PYT 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table NSA.DOC_ELECT_SAP
     *
     * @mbg.generated Mon Jun 18 13:34:19 PYT 2018
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}