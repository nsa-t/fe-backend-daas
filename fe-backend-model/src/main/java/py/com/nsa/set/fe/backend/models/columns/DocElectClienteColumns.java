package py.com.nsa.set.fe.backend.models.columns;

import java.util.HashMap;
import java.util.Map;

public final class DocElectClienteColumns {
    private static final Map<String, String> mapColumn = new HashMap<String, String>();

    static {
        mapColumn.put("idDocElectCliente","ID_DOC_ELECT_CLIENTE");
        mapColumn.put("fechaCreacion","FECHA_CREACION");
        mapColumn.put("codTipoDe","COD_TIPO_DE");
        mapColumn.put("fechaFirma","FECHA_FIRMA");
        mapColumn.put("fechaSet","FECHA_SET");
        mapColumn.put("aprobadoSet","APROBADO_SET");
        mapColumn.put("origen","ORIGEN");
        mapColumn.put("sxml","SXML");
        mapColumn.put("sxmlfirmado","SXMLFIRMADO");
    }

    public static String getColumnsName(String nombreAtributo) {
        return mapColumn.get(nombreAtributo);
    }
}