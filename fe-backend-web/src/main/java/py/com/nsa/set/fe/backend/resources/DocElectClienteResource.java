package py.com.nsa.set.fe.backend.resources;

import py.com.daas.mybatis.backendbase.resources.WritableResource;
import py.com.nsa.set.fe.backend.dao.DocElectClienteDAO;
import py.com.nsa.set.fe.backend.models.DocElectCliente;
import py.com.nsa.set.fe.backend.models.examples.DocElectClienteExample;
import py.com.nsa.set.fe.backend.models.mappers.DocElectClienteMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;

@RequestScoped
@Path(value = "elect-cliente")
public class DocElectClienteResource extends WritableResource<DocElectCliente, Short, DocElectClienteExample, DocElectClienteMapper, DocElectClienteDAO> {
    @Inject
    private DocElectClienteDAO dao;

    @Override
    protected DocElectClienteDAO getDao() {
        return dao;
    }

    @Override
    protected Class<DocElectCliente> getEntityBeanType() {
        return DocElectCliente.class;
    }

    @Override
    protected Class<Short> getEntityKeyType() {
        return Short.class;
    }

    @Override
    protected Class<DocElectClienteExample> getExampleType() {
        return DocElectClienteExample.class;
    }
}