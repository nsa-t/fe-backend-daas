package py.com.nsa.set.fe.backend.resources;

import py.com.daas.mybatis.backendbase.resources.WritableResource;
import py.com.nsa.set.fe.backend.dao.TipoDocumentoElectDAO;
import py.com.nsa.set.fe.backend.models.TipoDocumentoElect;
import py.com.nsa.set.fe.backend.models.examples.TipoDocumentoElectExample;
import py.com.nsa.set.fe.backend.models.mappers.TipoDocumentoElectMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;

@RequestScoped
@Path(value = "documento-elect")
public class TipoDocumentoElectResource extends WritableResource<TipoDocumentoElect, Short, TipoDocumentoElectExample, TipoDocumentoElectMapper, TipoDocumentoElectDAO> {
    @Inject
    private TipoDocumentoElectDAO dao;

    @Override
    protected TipoDocumentoElectDAO getDao() {
        return dao;
    }

    @Override
    protected Class<TipoDocumentoElect> getEntityBeanType() {
        return TipoDocumentoElect.class;
    }

    @Override
    protected Class<Short> getEntityKeyType() {
        return Short.class;
    }

    @Override
    protected Class<TipoDocumentoElectExample> getExampleType() {
        return TipoDocumentoElectExample.class;
    }
}