package py.com.nsa.set.fe.backend.resources;

import py.com.daas.mybatis.backendbase.resources.WritableResource;
import py.com.nsa.set.fe.backend.dao.DocElectSapDAO;
import py.com.nsa.set.fe.backend.models.DocElectSap;
import py.com.nsa.set.fe.backend.models.examples.DocElectSapExample;
import py.com.nsa.set.fe.backend.models.mappers.DocElectSapMapper;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Path;

@RequestScoped
@Path(value = "elect-sap")
public class DocElectSapResource extends WritableResource<DocElectSap, Short, DocElectSapExample, DocElectSapMapper, DocElectSapDAO> {
    @Inject
    private DocElectSapDAO dao;

    @Override
    protected DocElectSapDAO getDao() {
        return dao;
    }

    @Override
    protected Class<DocElectSap> getEntityBeanType() {
        return DocElectSap.class;
    }

    @Override
    protected Class<Short> getEntityKeyType() {
        return Short.class;
    }

    @Override
    protected Class<DocElectSapExample> getExampleType() {
        return DocElectSapExample.class;
    }
}