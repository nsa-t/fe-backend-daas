package py.com.nsa.set.fe.backend.resources;

import py.com.daas.mybatis.backendbase.resources.BaseResource;
import py.com.daas.util.exceptions.ApplicationException.IllegalArgument;
import py.com.daas.util.exceptions.ApplicationException.NotFound;
import py.com.daas.util.exceptions.ApplicationException.SQLException;
import py.com.nsa.set.fe.backend.models.params.fe.ProcesarCbteParam;
import py.com.nsa.set.fe.backend.services.FeService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
@Path("fe")
@RequestScoped
public class FeResource extends BaseResource {

    @Inject
    private FeService service;

    @POST
    @Path("procesar-cbte")
    public Response procesarCbte(ProcesarCbteParam param) throws IllegalArgument, SQLException, NotFound  {
        return Response.ok(service.procesarCbte(param)).build();
    }

}
