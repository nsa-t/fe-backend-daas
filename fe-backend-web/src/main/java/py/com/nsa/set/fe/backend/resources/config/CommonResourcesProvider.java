package py.com.nsa.set.fe.backend.resources.config;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import javax.enterprise.inject.Produces;
import java.text.SimpleDateFormat;

/**
 * Esta clase provee instancias de diversos recursos a ser utilizados, via
 * inyecci&oacute;n, a lo largo de las implementaciones de los Jax-rs root
 * resources
 *
 * @author Derlis Argüello <derlisarguello01@gmail.com>
 */
public class CommonResourcesProvider {

    /**
     * Retorna una instancia de la clase
     * {@code org.codehaus.jackson.map.ObjectMapper} a ser utilizada para
     * Serializaci&oacute;n/Deserializaci&oacute;n de Object a JSON y viceversa.
     *
     * @return ObjectMapper
     */
    @Produces
    @SuppressWarnings("deprecation")
    public ObjectMapper obtainJacksonObjectMapperInstance() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setDateFormat(new SimpleDateFormat(JacksonObjectMapperProvider.DATE_FORMAT));
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        return objectMapper;
    }
}
